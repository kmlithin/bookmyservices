@extends('layouts.app')
@section('content')
<div class="container-fluid mt-5">
    <div class="container">
        <div class="row">
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="{{ route('showRequests')}}">Requests</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" href="{{ route('showServices')}}">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('showPayments')}}">Payments</a>
                  </li>
            </ul>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        @foreach ($services as $item)
                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{ $loop->iteration-1 }}" class="active" aria-current="true" aria-label="Slide {{ $loop->iteration }}"></button>
                        @endforeach
                    </div>
                    <div class="carousel-inner">
                        @foreach ($services as $item)

                            <div class="carousel-item @if($loop->iteration == 1) active @endif" style="height: 250px">
                                <div class="container">
                                    <div class="row">
                                      <div class="col-md-8"><img src="{{ asset('services/'.$item->images) }}" class="d-block w-100 img-fluid" alt="..." width="250">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="m-2">
                                            <h3 >{{ $item->name }}</h3>
                                            <h5>{{ $item->categories->name }}</h4>
                                            <p>{{ $item->description }}</p>
                                            <i>For {{ $item->session }}</i>
                                            <b>${{ round($item->price,2) }}</b>
                                        </div>
                                    </div>
                                 </div>
                            </div>

                            </div>
                        @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"  data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"  data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                  </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                These are your upcoming services. You could scan your customer's QR Code before service to check-in, or scan QR code to generateinvoice for payments
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            @foreach ($bookings as $item)
            <div class="card m-2">

                <div class="card-body">
                    <div class="col-md-6 m-2">
                        <h5 class="card-title mt-2">Upcoming Service</h5>
                        <i>{{ $item->created_at }}</i>
                    </div>
                    <div class="col-md-6 m-2">
                        <i class="bi bi-check"></i>Services
                    </div>
                    <div class="col-md-4 m-2">
                        <img src="{{ asset('users/'.$item->users->images) }}" class="img-thumbnail wd-25"style="width: 50px;">
                        <b>{{ $item->users->name }}</b>
                        <i>{{ $item->users->location }}</i>
                    </div>
                    @if(count($item->availabilties)>0)
                    <div class="col-md-12 m-2">
                        <i>This customer is available at </i><br>
                        @foreach ($item->availabilties as $values)
                            <i></i><b>{{ date('D',strtotime($values->date)) }}, {{ date('M',strtotime($values->date)) }} {{ date('d',strtotime($values->date)) }} {{ date('Y',strtotime($values->date)) }}</b>
                              &nbsp;&nbsp;&nbsp; {{ date('H:i',strtotime($values->from)) }}-{{ date('H:i',strtotime($values->to)) }}
                        @endforeach
                    </div>
                    @endif
                    <div class="col-md-12">
                        <form method="POST" action="{{ route('changeStatus') }}">
                            @csrf
                            <button type="button" class="btn btn-outline-primary btn-lg">Check in</button>
                            <input type="hidden" value="payments" name="status">
                            <input type="hidden" value="{{ $item->id }}" name="id">
                            <button type="submit" class="btn btn-primary btn-lg">Generate Invoice</button>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @if($bookingCount>5)
            <button class="btn btn-outline-dark m-2" type="button">View More</button>
            @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
</script>
@endsection
