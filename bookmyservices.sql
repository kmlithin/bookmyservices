-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2021 at 09:04 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookmyservices`
--

-- --------------------------------------------------------

--
-- Table structure for table `availabilties`
--

CREATE TABLE `availabilties` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `from` time NOT NULL,
  `to` time NOT NULL,
  `service_booking` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `availabilties`
--

INSERT INTO `availabilties` (`id`, `date`, `from`, `to`, `service_booking`, `created_at`, `updated_at`) VALUES
(1, '2021-02-19', '02:20:43', '03:20:43', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Training and Fitness', '2021-02-18 14:10:25', '2021-02-18 14:10:25', NULL),
(2, 'Activities', '2021-02-18 14:10:25', '2021-02-18 14:10:25', NULL),
(3, 'Work', '2021-02-18 14:10:25', '2021-02-18 14:10:25', NULL),
(4, 'Training and Fitness', '2021-02-18 14:11:15', '2021-02-18 14:11:15', NULL),
(5, 'Activities', '2021-02-18 14:11:15', '2021-02-18 14:11:15', NULL),
(6, 'Work', '2021-02-18 14:11:15', '2021-02-18 14:11:15', NULL),
(7, 'Training and Fitness', '2021-02-18 14:15:49', '2021-02-18 14:15:49', NULL),
(8, 'Activities', '2021-02-18 14:15:49', '2021-02-18 14:15:49', NULL),
(9, 'Work', '2021-02-18 14:15:49', '2021-02-18 14:15:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(89, '2014_10_12_000000_create_users_table', 1),
(90, '2014_10_12_100000_create_password_resets_table', 1),
(91, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(92, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(93, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(94, '2016_06_01_000004_create_oauth_clients_table', 1),
(95, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(96, '2019_08_19_000000_create_failed_jobs_table', 1),
(97, '2021_02_18_160105_create_categories_table', 1),
(98, '2021_02_18_160106_create_services_table', 1),
(99, '2021_02_18_180256_create_service_bookings_table', 1),
(100, '2021_02_18_184808_create_availabilties_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('40e372cb539e7f7a8262d7274a3043d126ae0d0f2b7cb9d094936cb811ecc3631f0e32cf9c8a0c4a', 11, '92c35a4e-7338-445a-ab74-54649910222e', 'Personal Access Token', '[]', 1, '2021-02-18 14:12:14', '2021-02-18 14:12:14', '2021-02-25 19:42:14'),
('554e24ceb6ccd36bda763e5c0402350a5641a6648d9b18675ade51bebd64167e11ce20dcf20a2ceb', 11, '92c35a4e-7338-445a-ab74-54649910222e', 'Personal Access Token', '[]', 0, '2021-02-18 14:22:03', '2021-02-18 14:22:03', '2021-02-25 19:52:03'),
('56e0122a631501186e981533d43753d621ad1443e1c30c77ad91b5bd98e2cfe90a01fbc1fad92b18', 11, '92c35a4e-7338-445a-ab74-54649910222e', 'Personal Access Token', '[]', 0, '2021-02-18 14:25:40', '2021-02-18 14:25:40', '2022-02-18 19:55:40'),
('6243da63feb4d233c6906f954db29069ca212f5c8484b85dfd38c6566e29d7101a7bb68b4b406acc', 11, '92c35a4e-7338-445a-ab74-54649910222e', 'Personal Access Token', '[]', 0, '2021-02-18 14:12:47', '2021-02-18 14:12:47', '2021-02-25 19:42:47'),
('d39491015e192f52586f0641bfde7ed05e2adda9bfd8b490b8369b212697c36fe54182930e495646', 11, '92c35a4e-7338-445a-ab74-54649910222e', 'Personal Access Token', '[]', 0, '2021-02-18 14:28:42', '2021-02-18 14:28:42', '2022-02-18 19:58:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
('92c35a4e-7338-445a-ab74-54649910222e', NULL, 'Laravel Personal Access Client', 'z3LkmlLwXgdp8eOKIsKQRpixpERtb82XVzwOG3r9', NULL, 'http://localhost', 1, 0, 0, '2021-02-18 14:11:53', '2021-02-18 14:11:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, '92c35a4e-7338-445a-ab74-54649910222e', '2021-02-18 14:11:53', '2021-02-18 14:11:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `session` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `category`, `description`, `session`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Yoga and Pilat Training', 1, 'This yoga and pilate training session is designed for all type of age groups', 'One session', 80.00, '2021-02-18 14:11:15', NULL, NULL),
(2, 'Gym Sessions', 1, 'This Gym sessions training session is designed for all type of age groups', 'Two session', 126.00, '2021-02-18 14:11:15', NULL, NULL),
(3, 'Sports Training', 1, 'This Sports Training training session is designed for all type of age groups', 'Three session', 180.00, '2021-02-18 14:11:15', NULL, NULL),
(4, 'Yoga and Pilat Training', 1, 'This yoga and pilate training session is designed for all type of age groups', 'One session', 80.00, '2021-02-18 14:15:49', NULL, NULL),
(5, 'Gym Sessions', 1, 'This Gym sessions training session is designed for all type of age groups', 'Two session', 126.00, '2021-02-18 14:15:49', NULL, NULL),
(6, 'Sports Training', 1, 'This Sports Training training session is designed for all type of age groups', 'Three session', 180.00, '2021-02-18 14:15:49', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_bookings`
--

CREATE TABLE `service_bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` bigint(20) UNSIGNED NOT NULL,
  `service` bigint(20) UNSIGNED NOT NULL,
  `status` enum('requests','services','payments') COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_bookings`
--

INSERT INTO `service_bookings` (`id`, `user`, `service`, `status`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, 1, 'requests', '', '2021-02-15 19:43:51', NULL, NULL),
(2, 11, 2, 'requests', '', '2021-02-09 19:57:32', NULL, NULL),
(3, 11, 3, 'requests', '', '2021-02-15 19:43:51', NULL, NULL),
(4, 11, 4, 'requests', '', '2021-02-02 19:57:36', NULL, NULL),
(5, 11, 5, 'requests', '', '2021-02-15 19:43:51', NULL, NULL),
(6, 11, 6, 'requests', '', '2021-02-10 19:57:40', NULL, NULL),
(7, 11, 1, 'services', '', '2021-02-15 19:43:51', NULL, NULL),
(8, 11, 2, 'services', '', '2021-02-18 19:57:43', NULL, NULL),
(9, 11, 3, 'services', '', '2021-02-15 19:43:51', NULL, NULL),
(10, 11, 4, 'services', '', '2021-02-18 19:57:48', NULL, NULL),
(11, 11, 5, 'services', '', '2021-02-15 19:43:51', NULL, NULL),
(12, 11, 6, 'services', '', '2021-02-23 19:57:51', NULL, NULL),
(13, 11, 1, 'payments', '', '2021-02-15 19:43:51', NULL, NULL),
(14, 11, 2, 'payments', '', '2021-02-01 19:58:19', NULL, NULL),
(15, 11, 6, 'payments', '', '2021-02-15 19:46:04', NULL, NULL),
(16, 11, 3, 'payments', '', '2021-02-16 19:58:23', NULL, NULL),
(17, 11, 6, 'payments', '', '2021-02-15 19:46:04', NULL, NULL),
(18, 11, 3, 'services', '', '2021-02-23 19:57:54', NULL, NULL),
(19, 11, 6, 'requests', '', '2021-02-15 19:46:04', NULL, NULL),
(20, 11, 3, 'services', '', '2021-02-14 19:57:57', NULL, NULL),
(21, 11, 6, 'requests', '', '2021-02-15 19:46:04', NULL, NULL),
(22, 11, 3, 'services', '', '2021-02-16 19:58:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `location`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ottis Haley DVM', 'rutherford.nannie@example.com', 'Shirley Bashirian', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'uCQM8yEP1r', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(2, 'Samara Rolfson', 'emmanuelle87@example.org', 'Prof. Liam Price', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EggPQvtFxs', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(3, 'Dr. Noemi Kautzer', 'arely.schmidt@example.org', 'Melissa Kling Sr.', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UoPP47UYDa', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(4, 'Ulises Paucek', 'beier.vladimir@example.com', 'Makayla Hoeger', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'TCNiQoFV9A', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(5, 'Marjolaine O\'Connell', 'vmorar@example.net', 'Berta Kemmer', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6G2p8qWrJu', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(6, 'Dr. Angelina Rodriguez IV', 'zboyle@example.com', 'Mattie Wolf', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IHVP8jkrd3', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(7, 'Bud Hahn', 'ybalistreri@example.org', 'Lacy Ziemann', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aKSwoe73vv', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(8, 'Valentina Gibson', 'owolf@example.org', 'Marlee Orn MD', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'prwFYKPNWS', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(9, 'Brenda Prohaska IV', 'darby.kshlerin@example.com', 'Kathryn Breitenberg', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'H8C63U11EO', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(10, 'Daija Goodwin', 'rogahn.jerald@example.net', 'Marquis Stracke', '2021-02-18 14:11:15', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'gpuJxhwDza', '2021-02-18 14:11:15', '2021-02-18 14:11:15'),
(11, 'Lithin km', 'lithin@example.com', 'Kochin, Ernakulam', NULL, '$2y$10$yYd8YY0KEv5SVPNfPZYgVObvjFNfeE6kpgcLB.8WJYgTDDaFb5s4q', NULL, '2021-02-18 14:11:34', '2021-02-18 14:11:34'),
(12, 'Liana Volkman', 'joannie.rohan@example.net', 'Mr. Jules Medhurst PhD', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ODYX0iv26R', '2021-02-18 14:15:49', '2021-02-18 14:15:49'),
(13, 'Prof. Britney Runte Sr.', 'llangworth@example.org', 'Jermaine Bayer', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'mKmTGR7X5l', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(14, 'Trevor Hane', 'ludie.nienow@example.com', 'Dorothea Stoltenberg', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3UXzvPdNrw', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(15, 'Dr. Dante Roob Jr.', 'shaylee19@example.org', 'Tom Waelchi', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'pySPiXuZ1V', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(16, 'Prof. Cathrine Morar', 'xkessler@example.net', 'Dr. Betty Fahey', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lJYQTZOCxy', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(17, 'Shanna Bashirian', 'bartoletti.loraine@example.com', 'Lori Nienow', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NbYiE1uFGw', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(18, 'Dean Brekke', 'lavada44@example.net', 'Bruce Hilpert', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2qrwiRI6ek', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(19, 'Leonie Hirthe IV', 'walker.jordon@example.net', 'Trey Dach', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HHRFzoCAx2', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(20, 'Blanche Hartmann', 'hermann.wolf@example.org', 'Prof. Emanuel Aufderhar DDS', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kY27d8vD4B', '2021-02-18 14:15:50', '2021-02-18 14:15:50'),
(21, 'Vida Carter IV', 'rae74@example.org', 'Mrs. Estell Emard V', '2021-02-18 14:15:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4u4ZTzN8R5', '2021-02-18 14:15:50', '2021-02-18 14:15:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `availabilties`
--
ALTER TABLE `availabilties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `availabilties_service_booking_foreign` (`service_booking`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_category_foreign` (`category`);

--
-- Indexes for table `service_bookings`
--
ALTER TABLE `service_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_bookings_user_foreign` (`user`),
  ADD KEY `service_bookings_service_foreign` (`service`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `availabilties`
--
ALTER TABLE `availabilties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `service_bookings`
--
ALTER TABLE `service_bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `availabilties`
--
ALTER TABLE `availabilties`
  ADD CONSTRAINT `availabilties_service_booking_foreign` FOREIGN KEY (`service_booking`) REFERENCES `service_bookings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_category_foreign` FOREIGN KEY (`category`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_bookings`
--
ALTER TABLE `service_bookings`
  ADD CONSTRAINT `service_bookings_service_foreign` FOREIGN KEY (`service`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `service_bookings_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
