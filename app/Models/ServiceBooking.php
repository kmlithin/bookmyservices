<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceBooking extends Model
{
    use HasFactory;
    public function users(){
        return $this->hasOne(User::class,'id','user');
    }
    public function availabilties(){
        return $this->hasMany(Availabilty::class,'service_booking');
    }
}
