<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\ServiceBooking;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function showRequests(Request $request){
        $data['services'] = Service::orderBy('created_at','DESC')->get();
        $offset = ($request->page-1)*5;
        $data['bookings'] = ServiceBooking::orderBy('created_at','DESC')->where('status','requests')->skip($offset)->take(5)->with('users','availabilties')->get();
        $data['bookingCount'] = ServiceBooking::count();
        return view('showRequests',$data);
    }
    public function showServices(Request $request){
        $data['services'] = Service::orderBy('created_at','DESC')->get();
        $offset = ($request->page-1)*5;
        $data['bookings'] = ServiceBooking::orderBy('created_at','DESC')->where('status','services')->skip($offset)->take(5)->with('users','availabilties')->get();
        $data['bookingCount'] = ServiceBooking::count();
        return view('showServices',$data);
    }
    public function showPayments(Request $request){
        $data['services'] = Service::orderBy('created_at','DESC')->get();
        $offset = ($request->page-1)*5;
        $data['bookings'] = ServiceBooking::orderBy('created_at','DESC')->where('status','payments')->skip($offset)->take(5)->with('users','availabilties')->get();
        $data['bookingCount'] = ServiceBooking::count();
        return view('showPayments',$data);
    }
    public function changeStatus(Request $request){
        if($request->id && $request->status){
            if($request->status == 'requests' || $request->status == 'services' ||$request->status == 'payments'){
                if(ServiceBooking::where('id',$request->id)->update(['status'=>$request->status])){
                    if($request->status == 'services'){
                        return redirect('service');
                    }else if($request->status == 'payments'){
                        return redirect('payments');
                    }else{
                        return redirect('/');
                    }
                }else{
                    return response()->json(['status'=>0, 'message' =>'No request found']);
                }
            }else{
                return response()->json(['status'=>0, 'message' =>'Invalid status provided']);
            }
        }else{
            return response()->json(['status'=>0, 'message' =>'Please input Id and status']);
        }
    }
}
