<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function services(Request $request){
        $services = Service::orderBy('created_at','DESC')->get();
        return response()->json(['status'=>1, 'services' =>$services]);
    }
}
