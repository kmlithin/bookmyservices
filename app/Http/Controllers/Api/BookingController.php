<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ServiceBooking;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function bookings(Request $request){
        $offset = ($request->page-1)*5;
        $status = $request->status;
        $services = ServiceBooking::orderBy('created_at','DESC')->where('status',$status)->skip($offset)->take(5)->with('users','availabilties')->get();
        if(count($services)>0){
            return response()->json(['status'=>1, 'services' =>$services]);
        }else{
            return response()->json(['status'=>0, 'message' =>'No more service requests']);
        }
    }
    public function selectBooking(Request $request){
        if($request->id){
            $services = ServiceBooking::where('id',$request->id)->with('users','availabilties')->get();
            if(count($services)>0){
                return response()->json(['status'=>1, 'services' =>$services]);
            }else{
                return response()->json(['status'=>0, 'message' =>'No request found']);
            }
        }else{
            return response()->json(['status'=>0, 'message' =>'Please input Id']);
        }
    }
    public function changeStatus(Request $request){
        if($request->id && $request->status){
            if($request->status == 'requests' || $request->status == 'services' ||$request->status == 'payments'){
                if(ServiceBooking::where('id',$request->id)->update(['status'=>$request->status])){
                    return response()->json(['status'=>1, 'message' =>'Successfully updated']);
                }else{
                    return response()->json(['status'=>0, 'message' =>'No request found']);
                }
            }else{
                return response()->json(['status'=>0, 'message' =>'Invalid status provided']);
            }
        }else{
            return response()->json(['status'=>0, 'message' =>'Please input Id and status']);
        }
    }
}
