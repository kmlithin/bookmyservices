<?php

use App\Http\Controllers\RequestController;
use Illuminate\Support\Facades\Route;

Route::get('/', [RequestController::class,'showRequests'])->name('showRequests');
Route::get('/service', [RequestController::class,'showServices'])->name('showServices');
Route::get('/payments', [RequestController::class,'showPayments'])->name('showPayments');
Route::post('bookings/change-status', [RequestController::class,'changeStatus'])->name('changeStatus');
