<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailabiltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilties', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->time('from');
            $table->time('to');
            $table->unsignedBigInteger('service_booking');
            $table->foreign('service_booking')->references('id')->on('service_bookings')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availabilties');
    }
}
