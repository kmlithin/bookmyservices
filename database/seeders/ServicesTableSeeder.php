<?php

namespace Database\Seeders;

use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = DB::table('categories')->pluck('id');
        $rows = [
            ['name' => 'Yoga and Pilat Training',
            'category' => 1,
            'description' => 'This yoga and pilate training session is designed for all type of age groups',
            'session' => 'One session',
            'price' => '80.00',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' => 'Gym Sessions',
            'category' => 1,
            'description' => 'This Gym sessions training session is designed for all type of age groups',
            'session' => 'Two session',
            'price' => '126.00',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ['name' => 'Sports Training',
            'category' => 1,
            'description' => 'This Sports Training training session is designed for all type of age groups',
            'session' => 'Three session',
            'price' => '180.00',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')],
            ];
        DB::table('services')->insert($rows);
    }
}
